from django.test import TestCase

# Create your tests here.
from django.test import Client
from django.urls import resolve
class Lab8UnitTest(TestCase):
	def test_lab_8_url_is_exist(self):
		response = Client().get('/lab-8/')
		self.assertEqual(response.status_code, 200)

